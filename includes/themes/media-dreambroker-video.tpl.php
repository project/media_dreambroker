<?php

/**
 * @file media_dreambroker/includes/themes/media-dreambroker-video.tpl.php
 *
 * Template file for theme('media_dreambroker_video').
 *
 * Variables available:
 *  $uri - The media uri for the Dreambroker video (e.g., dreambroker://w/xsy7x8c9).
 *  $video_id - The unique identifier of the Dreambroker video (e.g., xsy7x8c9).
 *  $id - The file entity ID (fid).
 *  $url - The full url including query options for the Dreambroker iframe.
 *  $options - An array containing the Media Dreambroker formatter options.
 *  $width - The width value set in Media: Dreambroker file display options.
 *  $height - The height value set in Media: Dreambroker file display options.
 *  $bgimg - A transparent 16x9 png, that allows the responsive iframe to work.
 *
 */

?>
<div style="position:relative;" class="<?php print $classes; ?> media-dreambroker-<?php print $id; ?>">
  <img class="ratio" src="<?php print $bgimg; ?>" style="display:block;width:100%;height:auto;"/>
  <iframe style="position:absolute;top:0;left:0;width:100%; height:100%;" name="player-frame" class="media-dreambroker-player" width="<?php print $width; ?>" height="<?php print $height; ?>" src="<?php print $url; ?>" scrolling="no" frameborder="0" allowfullscreen webkitallowfullscreen mozallowfullscreen></iframe>
</div>