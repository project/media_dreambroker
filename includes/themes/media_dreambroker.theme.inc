<?php

/**
 * @file media_dreambroker/includes/themes/media_dreambroker.theme.inc
 *
 * Theme and preprocess functions for Media: Dreambroker.
 */

/**
 * Preprocess function for theme('media_dreambroker_video').
 */
function media_dreambroker_preprocess_media_dreambroker_video(&$variables) {

  // Build the URI.
  $wrapper = file_stream_wrapper_get_instance_by_uri($variables['uri']);
  $parts = $wrapper->get_parameters();
  $variables['video_id'] = $parts['w'];
  if (isset($parts['f'])) {
    $variables['channel'] = $parts['f'];
  }

  // Parse options and build the query string. Only add the option to the query
  // array if the option value is not default. Be careful, depending on the
  // wording in media_dreambroker.formatters.inc, TRUE may be query=0.
  $query = array();

  // These queries default to 0. If the option is true, set value to 1.
  foreach (array('autoplay', 'loop') as $option) {
    if ($variables['options'][$option]) {
      $query[$option] = 1;
    }
  }

  $variables['api_id_attribute'] = NULL;

  // These queries default to 1. If the option is false, set value to 0.
  foreach (array('portrait', 'title', 'byline') as $option) {
    if (!$variables['options'][$option]) {
      $query[$option] = 0;
    }
  }

  // Strings.
  if (isset($variables['options']['color'])) {
    $query['color'] = str_replace('#', '', $variables['options']['color']);
  }

  // Non-query options.
  $protocol = $variables['options']['protocol'];

  // Add some options as their own template variables.
  foreach (array('width', 'height') as $themevar) {
    $variables[$themevar] = $variables['options'][$themevar];
  }

  // Give the template a scalable background image to use under the iframe.
  $variables['bgimg'] = url(drupal_get_path('module', 'media_dreambroker') . "/images/video.png", array('absolute' => true));

  // Build the iframe URL with options query string.
  if (isset($variables['channel'])) {
    $variables['url'] = url($protocol . 'dreambroker.com/channel/' . $variables['channel'] . '/iframe/' . $variables['video_id'], array('query' => $query, 'external' => TRUE));
  } else {
    $variables['url'] = url($protocol . 'dreambroker.fi/w/' . $variables['video_id'], array('query' => $query, 'external' => TRUE));
  }
}
