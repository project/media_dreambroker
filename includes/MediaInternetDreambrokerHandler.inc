<?php

/**
 * Implementation of MediaInternetBaseHandler.
 *
 * @see hook_media_internet_providers().
 */
class MediaInternetDreambrokerHandler extends MediaInternetBaseHandler {
  public function parse($embedCode) {
    // Dreambroker has a few URL formats:
    //   http://dreambroker.fi/w/*
    //   https://www.dreambroker.fi/w/*
    //   https://www.dreambroker.fi/w/ba269e792
    //   https://dreambroker.com:443/channel/8vom6lp1/iframe/580aov03
    $patterns = array(
      '@dreambroker\.fi/w/([^\W]+)@i',
      '@dreambroker\.com/w/([^\W]+)@i',
      '@dreambroker\.com(:\d+)?/(channel/([^\W]+)/iframe/([^\W]+))@i',
      '@dreambroker\.fi(:\d+)?/(channel/([^\W]+)/iframe/([^\W]+))@i',
    );
    foreach ($patterns as $pattern) {
      preg_match($pattern, $embedCode, $matches);
      if (is_array($matches)) {
        $count = count($matches);

        if ($count == 5) {
          $folder = $matches[3];
          $video = $matches[4];
        } elseif ($count == 2) {
          $video = $matches[1];
        }
        if (isset($folder) && isset($video)) {
          return file_stream_wrapper_uri_normalize('dreambroker://w/' . $video . '/f/' . $folder);
        }
        elseif (!isset($folder) && isset($video)) {
          return file_stream_wrapper_uri_normalize('dreambroker://w/' . $video);
        }
      }
    }
  }

  public function claim($embedCode) {
    if ($this->parse($embedCode)) {
      return TRUE;
    }
  }

  public function getFileInformation() {

  }

  public function getFileObject() {
    $uri = $this->parse($this->embedCode);
    $file = file_uri_to_object($uri, TRUE);

    $wrapper = file_stream_wrapper_get_instance_by_uri($uri);
    $parts = $wrapper->get_parameters();

    if (isset($parts['f'])) {
      $video['channel'] = $parts['f'];
      $video['id'] = $parts['w'];
    }

    // Try to default the file name to the video's title.
    if (empty($file->fid) && isset($video) && $info = $this->getOEmbed($video)) {
      $file->filename = truncate_utf8($info['title'], 255);
    }
    return $file;
  }

  /**
   * Returns information about the media.
   *
   * @return
   *   If ATOM+MRSS information is available, a SimpleXML element containing
   *   ATOM and MRSS elements, as per those respective specifications.
   *
   * @todo Would be better for the return value to be an array rather than a
   *   SimpleXML element, but media_retrieve_xml() needs to be upgraded to
   *   handle namespaces first.
   */
  public function getMRSS() {
    $username = media_dreambroker_variable_get('username');
    $password = media_dreambroker_variable_get('password');
    $uri = $this->parse($this->embedCode);
    $video_id = arg(1, file_uri_target($uri));
    $rss_url = url('http://dreambroker.fi/api/1/rest/stats1?accessname=' . $username . '&password=' . $password . '&videoid=' . $video_id);
    // @todo Use media_retrieve_xml() once it's upgraded to include elements
    //   from all namespaces, not just the document default namespace.
    $entry = simplexml_load_file($rss_url);
    return $entry;
  }

  public function getOEmbed($video) {
    $oembed_url = url('http://dreambroker.com/channel/' . $video['channel'] . '.json');
    $response = drupal_http_request($oembed_url);
    if (!isset($response->error)) {
      $data = drupal_json_decode($response->data);
      foreach($data['items'] as $item) {
        if ($item['guid'] == $video['id']) {
          return $item;
        }
      }
    }
  }
}
