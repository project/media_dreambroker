<?php

/**
 *  @file
 *  Create a Dreambroker Stream Wrapper class for the Media/Resource module.
 */

/**
 *  Create an instance like this:
 *  $dreambroker = new ResourceDreambrokerStreamWrapper('dreambroker://w/[video-code]');
 */
class MediaDreambrokerStreamWrapper extends MediaReadOnlyStreamWrapper {
  static function getMimeType($uri, $mapping = NULL) {
    return 'video/dreambroker';
  }

  /**
   * Call the Dreambroker API and fetch the video information.
   *
   * @return
   *   Array of properties.
   */
  static function getVideoProperties($video_id) {
    $username = media_dreambroker_variable_get('username');
    $password = media_dreambroker_variable_get('password');
    $response = drupal_http_request('http://dreambroker.fi/api/1/rest/stats1?accessname=' . $username . '&password=' . $password . '&videoid=' . $video_id);
    #dsm($response, 'Response');
    $data = simplexml_load_string($response->data);
    #dsm($data, 'XML Data');
    return $data;
  }

  function getTarget($f) {
    return FALSE;
  }

  function interpolateUrl() {
    $video = $this->parameters['w'];
    if (isset($this->parameters['f'])) {
      $folder = $this->parameters['f'];
    }
    if (isset($folder)) {
      return 'http://dreambroker.fi/channel/' . $folder . '/iframe/' . $video;
    } else {
      return 'http://dreambroker.fi/w/' . $video;
    }
  }

  function getOriginalThumbnailPath() {
    if (isset($this->parameters['f'])) {
      $url = 'http://dreambroker.com/channel/' . $this->parameters['f'] . '.json';
      $file = drupal_http_request($url);
      $data = drupal_json_decode($file->data);
      foreach($data['items'] as $item) {
        if ($item['guid'] == $this->parameters['w']) {
          return $item['thumbnail'];
        }
      }
    }
    elseif (!isset($this->parameters['f'])) {
      $username = media_dreambroker_variable_get('username');
      $password = media_dreambroker_variable_get('password');
      $url = 'https://' . $username . ':' . $password . '@dreambroker.fi/g/' . $this->parameters['w'];
      $file = drupal_http_request($url);
      if ($file->data != "0") {
        return $url;
      } else {
        return "http://dreambroker.com/wp-content/themes/dreambroker/images/db-logo.png";
      }
    }
  }

  function getLocalThumbnailPath() {
    $local_path = 'public://media-dreambroker/' . $this->parameters['w'] . '.jpg';
    $original_path = $this->getOriginalThumbnailPath();

    if (!file_exists($local_path) && isset($original_path)) {
      $dirname = drupal_dirname($local_path);
      file_prepare_directory($dirname, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);
      @copy($original_path, $local_path);
    }
    return $local_path;
  }
}
